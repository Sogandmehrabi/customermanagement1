<%@ page import="dto.RealCustomer" %>

<%@ page import="java.util.List" %>
<%@ page import="enums.CustomerType" %><%--
Created by IntelliJ IDEA.
User: sogand
Date: 03/08/2020
Time: 22:48
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html la="fa">
<head>
    <meta charset="UTF-8">
    <title>لیست مشتریان</title>
    <link rel="stylesheet" type="text/css" href="css/register.css">
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>

<body bgcolor="#E6E6FA">
<table style="width:100%">
    <tr>
        <th>شماره مشتری</th>
        <th>نام</th>
        <th>نام خانوادگی</th>
        <th>نام پدر</th>
        <th>کد اقتصادی/کد ملی</th>
        <th>تاریخ ثبت/تاریخ تولد</th>
        <th>نوع مشتری</th>
        <th>حذف</th>
        <th>به روز رسانی</th>
    </tr>
    <c:forEach items="${realCustomers}" var="realCustomer">
        <tr>
            <td>${realCustomer.customerNumber}</td>
            <td>${realCustomer.name}</td>
            <td>${realCustomer.family}</td>
            <td>${realCustomer.fatherName}</td>
            <td>${realCustomer.code}</td>
            <td>${realCustomer.birth}</td>

            <td>حقیقی </td>
            <td><a href="<c:url value='/real-customer-list-delete?customerNumber=${realCustomer.customerNumber}'/>">حذف</a>
            </td>

            <td>
                <form id="u${realCustomer.customerNumber}" action="real-customer-list-update-routing" method="post" target="_blank">
                    <input type="hidden" name="customerNumber" value="${realCustomer.customerNumber}"/>
                    <a href="#" onclick="document.getElementById('u${realCustomer.customerNumber}').submit()">به روز رسانی</a>
                </form></tdu

        </tr>
    </c:forEach>
    <c:forEach items="${legalCustomers}" var="legalCustomer">
        <tr>
            <td>${legalCustomer.customerNumber}</td>
            <td>${legalCustomer.name}</td>
            <td></td>
            <td></td>
            <td>${legalCustomer.code}</td>
            <td>${legalCustomer.birth}</td>
            <td>حقوقی</td>
            <td><a href="<c:url value='/legal-customer-list-delete?customerNumber=${legalCustomer.customerNumber}'/>">حذف</a></td>
            <td>
                <form id="u${legalCustomer.customerNumber}" action="legal-customer-list-update-routing" method="post" target="_blank">
                    <input type="hidden" name="customerNumber" value="${legalCustomer.customerNumber}"/>
                    <a href="#" onclick="document.getElementById('u${legalCustomer.customerNumber}').submit()">به روز رسانی</a>
                </form></td>
        </tr>
    </c:forEach>

</table>
<br/><br/>
<a href="index.html" id="bz">بازگشت به صفحه اول</a>
</body>
</html>