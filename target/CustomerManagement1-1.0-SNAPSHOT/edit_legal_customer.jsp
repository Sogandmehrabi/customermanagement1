<%@ page import="dto.RealCustomer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>به روز رسانی</title>
    <link rel="stylesheet" type="text/css" href="css/register.css">
</head>
<body bgcolor="#E6E6FA">

<div id="div_form">

    <form name="regi" action="legal-customer-list-update" method="post" accept-charset="UTF-8" dir="rtl">
     &nbsp <div id="title"><h2>به روز رسانی مشتری</h2></div>

        <div> <label for="r6" id="cn">شماره مشتری </label><input  type="text" name="customerNumber"  value="${legalCustomer.customerNumber}" readonly id="r6" style="margin-left: 15px"></div>
        </br>

        <div><label for="r1" id="fn">نام شرکت </label><input type="text" name="name"  value="${legalCustomer.name}" id="r1" style="margin-right: 5px"></div>
        <br/>



        <div> <label for="r4" id="bd">تاریخ ثبت </label><input type="text" name="birth" value="${legalCustomer.birth}" id="r4" style="margin-right: 5px"></div>
        <br/>


        <div> <label for="r10" id="code">کد اقتصادی </label><input type="text" name="code" id="r10" value="${legalCustomer.code}"></div>
        <br/>

        <div> <label for="r7" id="ct">نوع مشتری </label> <input  type="text" name="customerType"  value="حقوقی" readonly id="r7" disabled> </div>
        </br>
        &nbsp &nbsp &nbsp<button type="submit" value="Submit" id="button">به روزرسانی مشتری</button>

    </form>

</div>
</center>
</body>
</html>
