<%@ page import="dto.LegalCustomer" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html la="fa">
<head>
    <meta charset="UTF-8">
    <title>Login and Registration form example</title>
    <link rel="stylesheet" type="text/css" href="css/register.css">
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>

<body bgcolor="#E6E6FA">

<table style="width:100%">
    <tr>
        <th>شماره مشتری</th>
        <th>نام شرکت</th>
        <th>کد اقتصادی</th>
        <th>تاریخ ثبت</th>
        <th>نوع مشتری</th>
        <th>حذف</th>
        <th>به روز رسانی</th>
    </tr>
    <c:forEach items="${legalCustomers}" var="legalCustomer">
        <tr>
            <td>${legalCustomer.customerNumber}</td>
            <td>${legalCustomer.name}</td>
            <td>${legalCustomer.code}</td>
            <td>${legalCustomer.birth}</td>
            <td>حقوقی</td>
            <td><a href="<c:url value='/legal-customer-list-delete?customerNumber=${legalCustomer.customerNumber}'/>">حذف</a></td>
            <td>
                <form id="postRequest" action="legal-customer-list-update-routing" method="post" target="_blank">
                    <input type="hidden" name="customerNumber" value="${legalCustomer.customerNumber}"/>
                    <a href="#" onclick="document.getElementById('postRequest').submit()">به روز رسانی</a>
                </form></td>
        </tr>
    </c:forEach>

</table>

</body>
</html>

