package dto;

import enums.CustomerType;

public class RealCustomer extends Customer {

    private String family;
    private String fatherName;

    public RealCustomer() {
    }

    public RealCustomer(String name, String code, String birth, String lastName, String fatherName) {
        super(name, code, birth);
        this.family = lastName;
        this.fatherName = fatherName;
        this.customerType = CustomerType.REAL;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String lastName) {
        this.family = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }
}