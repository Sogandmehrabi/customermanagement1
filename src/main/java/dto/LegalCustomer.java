package dto;

import enums.CustomerType;

public class LegalCustomer extends Customer {

    public LegalCustomer() {
    }

    public LegalCustomer(String name, String code, String birth) {
        super(name, code, birth);
        this.customerType= CustomerType.LEGAL;
    }
}
