package repository;

import dto.LegalCustomer;
import dto.RealCustomer;
import enums.CustomerType;
import exception.CodeException;
import utility.TextHelper;
import utility.calender.GeneralBase;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class LegalCustomerRepository {

    public int insertLegalCustomer(LegalCustomer legalCustomer) throws Exception {
        String INSERT_USERS_SQL = "INSERT INTO customer" +
                "  (f_c_name, family, father_name, n_c_code, birth,is_real) VALUES " +
                " (?, ?, ?, ?, ?,?);";


        int result = 0;

        Class.forName("com.mysql.jdbc.Driver");


        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");

            String englishBirth = TextHelper.changeDigitsToEnglish(legalCustomer.getBirth());
            LocalDate localDate = GeneralBase.getFromString(englishBirth);

            String englishCode = TextHelper.changeDigitsToEnglish(legalCustomer.getCode());
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL);
            preparedStatement.setString(1, legalCustomer.getName());
            preparedStatement.setString(2, null);
            preparedStatement.setString(3, null);
            preparedStatement.setString(4, englishCode);
            preparedStatement.setString(5, localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            preparedStatement.setBoolean(6, CustomerType.LEGAL.equals(legalCustomer.getCustomerType()) ? false : true);


    System.out.println(preparedStatement);
    // Step 3: Execute the query or update query
    result = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            // process sql exception
            printSQLException(e);
         //   throw new CodeException("this is not allowed");

        }
        return result;
    }

    public String getCustomerNumber(String code) throws Exception {

        Class.forName("com.mysql.jdbc.Driver");

        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String SELECT_SQL = "select * from customer where n_c_code = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(SELECT_SQL);

            selectPreparedStatement1.setString(1, code);

            ResultSet resultSet = selectPreparedStatement1.executeQuery();

            while (resultSet.next()) {

                return resultSet.getString("customer_number");

            }
        } catch (SQLException e) {
            // process sql exception
            printSQLException(e);
        }

        return "";


    }

    public List<LegalCustomer> getList(Long customerNumber, String firstNamePattern, String nationalCodePattern,String birthdayPattern1,String birthdayPattern2) {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            String selectQuery = "select * from customer c  where\n" +
                    "( ? is null or c.f_c_name like ? ) and\n" +
                    "( ? is null or c.n_c_code  = ? )  and\n" +
                    "( ? is null or c.customer_number = ?) and\n" +
                    "( ? is null or c.birth>=? ) and\n " +
                    "( ? is null  or  c.birth<=?) and\n" +
                    "( c.is_real = 0)\n";


            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            PreparedStatement selectPreparedStatement = connection.prepareStatement(selectQuery);

            selectPreparedStatement.setString(1, pattern(firstNamePattern));
            selectPreparedStatement.setString(2, pattern(firstNamePattern));
            selectPreparedStatement.setString(3, pattern(nationalCodePattern));
            selectPreparedStatement.setString(4, pattern(nationalCodePattern));
            if (customerNumber == null) {
                selectPreparedStatement.setString(5, null);
                selectPreparedStatement.setString(6, null);
            } else {
                selectPreparedStatement.setString(5, String.valueOf(customerNumber));
                selectPreparedStatement.setString(6, String.valueOf(customerNumber));

            }
            if (birthdayPattern1 != null) {
                String englishBirth = TextHelper.changeDigitsToEnglish(birthdayPattern1);
                LocalDate localDate = GeneralBase.getFromString(englishBirth);
                birthdayPattern1=localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            }
            selectPreparedStatement.setString(7, birthdayPattern1);
            selectPreparedStatement.setString(8, birthdayPattern1);
            if (birthdayPattern2 != null) {
                String englishBirth = TextHelper.changeDigitsToEnglish(birthdayPattern2);
                LocalDate localDate = GeneralBase.getFromString(englishBirth);
                birthdayPattern2=localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            }
            selectPreparedStatement.setString(9, birthdayPattern2);
            selectPreparedStatement.setString(10, birthdayPattern2);


            System.out.println(selectPreparedStatement.toString());

            ResultSet resultSet = selectPreparedStatement.executeQuery();

            List<LegalCustomer> legalCustomers = new LinkedList<>();

            while (resultSet.next()) {

               LegalCustomer legalCustomer = new LegalCustomer();

                legalCustomer.setCustomerNumber(resultSet.getInt("customer_number"));
                legalCustomer.setName(resultSet.getString("f_c_name"));
                legalCustomer.setCode(resultSet.getString("n_c_code"));
                Date date = resultSet.getDate("birth");

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = dateFormat.format(date);

                LocalDate localDate = LocalDate.parse(strDate);
                legalCustomer.setBirth(GeneralBase.convertLocalDateToShamsi(localDate));
                legalCustomer.setCustomerType(CustomerType.LEGAL);

              legalCustomers.add(legalCustomer);

            }
            connection.close();
            return legalCustomers;
        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getStackTrace());
            return Collections.emptyList();
        } finally {

        }
    }

    public LegalCustomer getLegalCustomer(Long customerNumber) {


        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String SELECT_SQL = "select * from customer where customer_number = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(SELECT_SQL);

            selectPreparedStatement1.setInt(1, customerNumber.intValue());

            ResultSet resultSet = selectPreparedStatement1.executeQuery();

            while (resultSet.next()) {

                LegalCustomer legalCustomer = new LegalCustomer();

                legalCustomer.setCustomerNumber(resultSet.getInt("customer_number"));
                legalCustomer.setName(resultSet.getString("f_c_name"));
                legalCustomer.setCode(resultSet.getString("n_c_code"));
                Date date = resultSet.getDate("birth");

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = dateFormat.format(date);

                LocalDate localDate = LocalDate.parse(strDate);
                legalCustomer.setBirth(GeneralBase.convertLocalDateToShamsi(localDate));
                legalCustomer.setCustomerType(CustomerType.LEGAL);


                connection.close();
                return legalCustomer;

            }
        } catch (Exception e) {
            // process sql exception

            System.out.println(e.getStackTrace());
        }
        return null;
    }

    public void deleteLegalCustomer(Long customer_number) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?allowPublicKeyRetrieval=true&useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String REMOVE_SQL = "delete from customer where `customer_number` = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(REMOVE_SQL);

            selectPreparedStatement1.setInt(1, customer_number.intValue());

            System.out.println(selectPreparedStatement1.toString());

            int result = selectPreparedStatement1.executeUpdate();
            connection.close();

        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getStackTrace());
        }
    }

    public void updateLegalCustomer(LegalCustomer legalCustomer) {

        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?allowPublicKeyRetrieval=true&useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");

            String UPDATE_SQL = "UPDATE customer c  " +
                    " set c.n_c_code = ?,  c.f_c_name = ?, c.birth = ? " +
                    " where c.customer_number = ?;";

            String englishBirth = TextHelper.changeDigitsToEnglish(legalCustomer.getBirth());
            LocalDate localDate = GeneralBase.getFromString(englishBirth);

            String englishCode = TextHelper.changeDigitsToEnglish(legalCustomer.getCode());
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, englishCode);

            preparedStatement.setString(2, legalCustomer.getName());
            preparedStatement.setString(3, localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

            preparedStatement.setInt(4, legalCustomer.getCustomerNumber());

            System.out.println(preparedStatement.toString());

            int result = preparedStatement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getMessage());
        }

    }

    private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private String pattern(String str) {
        if (str == null) {
            return str;
        }

        return "%".concat(str).concat("%");
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}


