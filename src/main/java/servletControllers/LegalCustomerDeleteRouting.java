package servletControllers;

import service.LegalCustomerService;
import service.RealCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/legal-customer-list-delete")
public class LegalCustomerDeleteRouting extends HttpServlet {

    private LegalCustomerService legalCustomerService;

    @Override
    public void init() throws ServletException {
        legalCustomerService = new LegalCustomerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));

        try {
            legalCustomerService.deleteLegalCustomer(customerNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }

        resp.sendRedirect("/CustomerManagement1_war_exploded/all-customer-list");
    }
}
