package servletControllers;

import dto.RealCustomer;
import exception.CodeException;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/real-customer-list-update")
public class RealCustomerUpdate extends HttpServlet {

    private RealCustomerService realCustomerService;

    @Override
    public void init() throws ServletException {
        realCustomerService = new RealCustomerService();
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastName");
        String fatherName = req.getParameter("fatherName");
        String birth = req.getParameter("birth");
        String code = req.getParameter("code");

        RealCustomer realCustomer = new RealCustomer();

        realCustomer.setCustomerNumber(customerNumber.intValue());
        realCustomer.setName(name);
        realCustomer.setFamily(lastName);
        realCustomer.setFatherName(fatherName);
        realCustomer.setBirth(birth);
        realCustomer.setCode(code);

        try {
            try {
                realCustomerService.updateRealCustomer(realCustomer);
            }
            catch (CodeException e) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("error_real.jsp");
                dispatcher.forward(req,resp);
                e.getMessage();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/CustomerManagement1_war_exploded/all-customer-list");
    }
}
