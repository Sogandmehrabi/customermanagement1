package servletControllers;

import service.RealCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/real-customer-list-delete")
public class RealCustomerDeleteRouting extends HttpServlet {

    private RealCustomerService realCustomerService;

    @Override
    public void init() throws ServletException {
        realCustomerService = new RealCustomerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));

        try {
            realCustomerService.deleteRealCustomer(customerNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }

        resp.sendRedirect("/CustomerManagement1_war_exploded/all-customer-list");
    }
}
