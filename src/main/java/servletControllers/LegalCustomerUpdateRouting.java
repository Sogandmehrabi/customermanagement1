package servletControllers;

import dto.LegalCustomer;
import dto.RealCustomer;
import service.LegalCustomerService;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/legal-customer-list-update-routing")
public class LegalCustomerUpdateRouting extends HttpServlet {

    private LegalCustomerService legalCustomerService;

    @Override
    public void init() throws ServletException {
        legalCustomerService = new LegalCustomerService();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));

        try {
            LegalCustomer legalCustomer = legalCustomerService.getLegalCustomer(customerNumber);
            req.setAttribute("legalCustomer", legalCustomer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("edit_legal_customer.jsp");
        dispatcher.forward(req, resp);
    }
}
