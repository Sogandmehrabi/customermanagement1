package servletControllers;

import dto.LegalCustomer;
import dto.RealCustomer;
import service.LegalCustomerService;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/legal-customer-list")
public class LegalCustomerListServlet extends HttpServlet {

    private LegalCustomerService legalCustomerService;

    @Override
    public void init() throws ServletException {
        legalCustomerService = new LegalCustomerService();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstNamePattern = req.getParameter("firstNamePattern");
        if (firstNamePattern != null) {
            if (firstNamePattern.equals("") ) {
                firstNamePattern = null;
            }
        }

        String nationalCodePattern = req.getParameter("nationalCodePattern");
        if (nationalCodePattern != null) {
            if (nationalCodePattern.equals("") ) {
                nationalCodePattern = null;
            }
        }
        String customerNumberStr = req.getParameter("customerNumber");
        if (customerNumberStr != null) {
            if (customerNumberStr.equals("")) {
                customerNumberStr = null;
            }
        }
        Long customerNumber = null;

        if (customerNumberStr != null) {
            customerNumber = Long.valueOf(customerNumberStr);
        }
        String birthdayPattern1=req.getParameter("birthdayPattern1");
        if (birthdayPattern1 != null) {
            if (birthdayPattern1.equals("")) {
                birthdayPattern1 = null;
            }
        }
        String birthdayPattern2=req.getParameter("birthdayPattern2");
        if (birthdayPattern2 != null) {
            if (birthdayPattern2.equals("")) {
                birthdayPattern2 = null;
            }
        }

        List<LegalCustomer> legalCustomers = legalCustomerService.getList(customerNumber,firstNamePattern,nationalCodePattern,birthdayPattern1,birthdayPattern2);

        req.setAttribute("legalCustomers", legalCustomers);
        RequestDispatcher dispatcher = req.getRequestDispatcher("legalCustomerList.jsp");
        dispatcher.forward(req, resp);
    }
}
