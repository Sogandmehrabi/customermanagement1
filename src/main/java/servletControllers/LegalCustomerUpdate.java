package servletControllers;

import dto.LegalCustomer;
import dto.RealCustomer;
import exception.CodeException;
import service.LegalCustomerService;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/legal-customer-list-update")
public class LegalCustomerUpdate extends HttpServlet {

    private LegalCustomerService legalCustomerService;

    @Override
    public void init() throws ServletException {
        legalCustomerService = new LegalCustomerService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));
        String name = req.getParameter("name");
        String birth = req.getParameter("birth");
        String code = req.getParameter("code");

        LegalCustomer legalCustomer = new LegalCustomer();

        legalCustomer.setCustomerNumber(customerNumber.intValue());
        legalCustomer.setName(name);
        legalCustomer.setBirth(birth);
        legalCustomer.setCode(code);

        try {
            try {
                legalCustomerService.updateLegalCustomer(legalCustomer);
            }
            catch (CodeException e) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("error.jsp");
                dispatcher.forward(req,resp);
                e.getMessage();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/CustomerManagement1_war_exploded/all-customer-list");
    }
}
