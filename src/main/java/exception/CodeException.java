package exception;

public class CodeException extends Exception {
    private String message;
    public CodeException(String message){
        this.message=message;
    }
    public String getMessage(){
        return message;
    }
}
